# Truncate all table records
TRUNCATE `tourofheroes`.`hero`;

# Insert Demo data
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(1,"Green Lantern");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(2,"Batman");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(3,"Superman");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(4,"ProfesorSuper-O");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(5,"Lion-O");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(6,"MoGo");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(7,"Shazam");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(8,"StarLord");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(9,"Wolvorine");
INSERT INTO `tourofheroes`.`hero` (`id`,`name`) VALUES(10,"Jhony Sins");
