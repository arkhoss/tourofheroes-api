package com.tourofheroes.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import com.tourofheroes.model.Hero;
import com.tourofheroes.service.HeroService;

@RestController
@RequestMapping("/api")
public class HeroController {
    
    @Autowired
    HeroService heroService;

    /* Save Hero */
    @PostMapping("/hero-create")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Create Hero", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Hero Created", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public Hero createHero(@Valid @RequestBody Hero hero){
        return heroService.saveHero(hero);
    }

    /* Get ALl Heroes */
    @GetMapping("/hero")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Get All Heroes", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Heroes found", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public List<Hero> getAllHeroes(){
        return heroService.findALl();
    }

    /* Get Hero by id */
    @GetMapping("/hero/{id}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Get Hero by ID", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Hero found", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<Hero> getHeroById(@PathVariable(value="id") Long heroId){

        Hero hero = heroService.findOne(heroId);

        if(hero==null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(hero);
    }

    /* Get Hero by name */
    @GetMapping("/hero-name/{name}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Get Hero by Name", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Hero found", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<Hero> getHeroByName(@PathVariable(value = "name") String heroName){

        Hero hero = heroService.findOneByName(heroName);

        if(hero==null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(hero);
    }

    /* Update Hero by id */
    @PutMapping(value="/hero-update/{id}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Update Hero", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Hero updated", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<Hero> updateHero(@PathVariable(value="id") Long heroId,@Valid @RequestBody Hero heroDetails) {

        Hero hero=heroService.findOne(heroId);
        if(hero==null){
            return ResponseEntity.notFound().build();
        }

        // new values of the object hero
        hero.setId(heroDetails.getId());
        hero.setName(heroDetails.getName());

        Hero updateHero=heroService.saveHero(hero);
        return ResponseEntity.ok().body(updateHero);
    }

	/* Delete an Hero by id */
    @DeleteMapping("/hero-delete/{id}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Hero deleted", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Hero deleted", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<Hero> deleteHero(@PathVariable(value="id") Long heroId){
		
		Hero hero=heroService.findOne(heroId);
		if(hero==null) {
			return ResponseEntity.notFound().build();
		}
		heroService.deleteHero(hero);		
		return ResponseEntity.ok().build();		
	}
}