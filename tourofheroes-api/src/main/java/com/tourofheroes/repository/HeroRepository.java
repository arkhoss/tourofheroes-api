package com.tourofheroes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.tourofheroes.model.Hero;

import java.util.Optional;

public interface HeroRepository extends JpaRepository<Hero, Long>{
    
    public Optional<Hero> findByName(String name);
}