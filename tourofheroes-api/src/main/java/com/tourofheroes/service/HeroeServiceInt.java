package com.tourofheroes.service;

import java.util.List;

import com.tourofheroes.model.Hero;

public interface HeroeServiceInt {

    /* Save Heroe */
    public Hero saveHero(Hero hero);

    /* Find All Heroes */
    public List<Hero> findALl();

    /* Get Hero by id */
    public Hero findOne(Long id);

    /* Get Hero by name */
    public Hero findOneByName(String name);

    /* Delete a Hero */
    public void deleteHero(Hero hero);
}
