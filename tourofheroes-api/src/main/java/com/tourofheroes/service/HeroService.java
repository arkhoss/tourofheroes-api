package com.tourofheroes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tourofheroes.model.Hero;
import com.tourofheroes.repository.HeroRepository;

import java.util.List;
import java.util.Optional;

@Service
public class HeroService implements HeroeServiceInt{

    @Autowired
    HeroRepository heroRepository;

    @Override
    public Hero saveHero(Hero hero) {
        return heroRepository.save(hero);
    }

    @Override
    public List<Hero> findALl() {
        return heroRepository.findAll();
    }

    @Override
    public Hero findOne(Long heroid) {
        return heroRepository.findById(heroid).orElse(null);
    }

    @Override
    public Hero findOneByName(String heroname) {
        Optional<Hero> posibleHero = heroRepository.findByName(heroname);
        if(posibleHero.isPresent()){
            return posibleHero.get();
        }else {
            return null;
        }
    }

    @Override
    public void deleteHero(Hero hero) {
        heroRepository.delete(hero);
    }

}