# Tourofheroes API

This project was generated to work as DB repository for ThourOfHeroes APP

## How to run this repo
Single container
```
docker run -d --restart=unless-stopped --log-driver json-file --net="host" --expose=5000/tcp -p 5000:5000 --name tourofheroes-api_api_1 arkhoss/tourofheroes-api
```

Run
```
  docker-compose up -d
```

Stop and Delete
```
  docker-compose down
```

## How to login to mySQL db
```
docker exec -ti tourofheroes-api_db_1 mysql -u root -p
```

## How to run the API
>TODO: add here steps to run API

## Author
David Caballero
